#!/usr/bin/env python3
import sys
import os
import argparse
import subprocess
import configparser


def getStdout(command, autosplit):
    if autosplit:
        return subprocess.run(command.split(), stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
    else:
        return subprocess.run(command, stdout=subprocess.PIPE).stdout.decode('utf-8').strip()


def loadSessions(config):
    # Get the old session list
    oldSessionList = getStdout(
        "qdbus org.kde.yakuake /yakuake/sessions sessionIdList", True).split(',')

    # Add the new sessions
    for session in config.sections():
        # Add new session
        sessionId = getStdout(
            "qdbus org.kde.yakuake /yakuake/sessions addSession", True)
        # Rename
        getStdout(["qdbus", "org.kde.yakuake", "/yakuake/tabs",
                   "setTabTitle", sessionId, config[session]["title"]], False)
        # cd to cwd
        getStdout(["qdbus", "org.kde.yakuake", "/yakuake/sessions",
                   "runCommandInTerminal", sessionId, " cd " + config[session]["cwd"]], False)

    # Remove the previous sessions
    for session in oldSessionList:
        getStdout(
            "qdbus org.kde.yakuake /yakuake/sessions removeSession " + session, True)


def parseSessions():
    # first get the session count
    config = configparser.ConfigParser()
    sessionList = getStdout(
        "qdbus org.kde.yakuake /yakuake/sessions sessionIdList", True).split(',')
    # Get session name and working directory
    for i in range(len(sessionList)):
        session = getStdout(
            "qdbus org.kde.yakuake /yakuake/tabs sessionAtTab " + str(i), True)
        title = getStdout(
            "qdbus org.kde.yakuake /yakuake/tabs tabTitle " + session, True)
        pid = getStdout("qdbus org.kde.yakuake /Sessions/" +
                        str(int(session)+1) + " processId", True)
        cwd = getStdout("pwdx " + pid, True).split()[1]
        config["Tab " + str(i)] = {"title": title,
                                   "cwd":   cwd}
    return config


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Store and set yakuake sessions")
    parser.add_argument("-i", "--in-file", dest="infile",
                        help='File to read from, or "-" for stdin', metavar='FILE')
    parser.add_argument("-o", "--out-file", dest="outfile",
                        help='File to write to, or "-" for stdout', metavar='FILE')

    args = parser.parse_args()
    if(args.infile == None and args.outfile == None):
        # print out the current session
        config = parseSessions()
        config.write(sys.stdout)
        sys.exit(0)
    if(args.infile):
        # read in the file and restore session
        if(not os.path.exists(args.infile)):
            print("File not found")
            sys.exit(1)
        config = configparser.ConfigParser()
        config.read(args.infile)
        loadSessions(config)
        sys.exit(0)
    if(args.outfile):
        # output the session file
        config = parseSessions()
        config.write(open(args.outfile, "w"))
        sys.exit(0)
